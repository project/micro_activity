
/**
 * MicroActivity object.
 */
var MicroActivity = MicroActivity || {};
MicroActivity.busy = false;
MicroActivity.activeElement = null;
MicroActivity.wrapper = null;
MicroActivity.selectors = MicroActivity.selectors || {};

/**
 * Bind behaviors to the selected controls.
 */
MicroActivity.bindControls = function(uid, $element) {
  $element.bind('mouseover', function() {
    if (!MicroActivity.busy) {
      MicroActivity.activeElement = {
        x: $element.position().top + $element.height(), 
        y: $element.position().left + $element.width()
      };
      $.post(Drupal.settings.basePath + 'heartbeat/microactivity/0/' + uid, {block: 0, ajax: 1}, MicroActivity.appendMessages);
      MicroActivity.busy = true;
    }
  });
  MicroActivity.wrapper.bind('mouseover', function() {
    MicroActivity.busy = true;
  });
  $element.bind('mouseout', function() {
    MicroActivity.busy = false;
  });
  MicroActivity.wrapper.bind('mouseout', function() {
    MicroActivity.busy = false;
  });
  setInterval(function(){ MicroActivity.hideMicroActivity(); }, 800);
}

/**
 * Append messages in the overlay wrapper.
 */
MicroActivity.appendMessages = function(data) {
  var result = Drupal.parseJson(data);
  MicroActivity.wrapper.html(result['data']);
  MicroActivity.wrapper.css({'top': MicroActivity.activeElement.x, 'left': MicroActivity.activeElement.y});
  MicroActivity.wrapper.show('slow');
}

/**
 * Hide the overlay wrapper.
 */
MicroActivity.hideMicroActivity = function() {
  if (!MicroActivity.busy) {
    MicroActivity.wrapper.html('').hide('slow');
    MicroActivity.busy = false;
  }
}

/**
 * Implements MicroActivity.selectors.hook.execute().
 * 
 * The html structure that is logged in the message template
 * gives the correct info to bind controls with arguments.
 * 
 * Structure in this example:
 *   <span class="micro-activity micro-activity-11">
 *     <a class="user" href="/user/11">User3</a>
 *   </span>
 * "11" is the user id that is split out as arg.
 */
MicroActivity.selectors.microActivity = {
  execute : function() {
    // Test to trigger it in the stream.
    $('span.micro-activity').each(function() {
      var $element = $(this);
      var classes = $element.attr('class').split(' ');
      for (n in classes) {
        var uid = classes[n].replace('micro-activity-', '');
        if (parseInt(uid) == uid) {
          MicroActivity.bindControls(uid, $('a', $element));
        }
      }
    });
  }
}

/**
 * Add behaviors for the page.
 */
Drupal.behaviors.MicroActivity = function (context) {

  // Prepare the overlay wrapper.
  MicroActivity.wrapper = $('<div id="micro-activity-overlay"></div>');
  $('body').append(MicroActivity.wrapper);

  // Let other javascript decide whether they want to join in on selectors.
  $.each(MicroActivity.selectors, function (func) {
    if ($.isFunction(this.execute)) {
      this.execute();
    }
  });
}