<?php

/**
 * @file
 * Micro activity stream object.
 */

heartbeat_include('HeartbeatAccess', 'heartbeat');

/**
 * Class MicroActivity
 *  Concrete class to prepare messages for the current user.
 */
class MicroActivity extends HeartbeatAccess {

  /**
   * Skip active user.
   * We never want to skip the active user. not for
   * logged-in user and not for displayed user (profile).
   * This is ofcourse because private stream is intended to
   * show own activity.
   */
  public function skipActiveUser() {
    return FALSE;
  }

  /**
   * finishMessages
   * Override to make sure there are no attachments loaded.
   * @param HeartbeatParser $heartbeat Parser object.
   * @return HeartbeatParser object
   */
  protected function finishMessages(HeartbeatParser & $heartbeat) {
    // Delete the attachments settings so they won't be read.
    foreach ($heartbeat->raw_messages as $key => $message) {
      $heartbeat->raw_messages[$key]->template->attachments = array();
    }
    return $heartbeat;
  }

  /**
   * Public function to dressup the request for messages.
   * @param HeartbeatParser $heartbeat Parser object.
   */
  public function dressUpMessages(HeartbeatParser $heartbeat) {
    $sql = " AND ua.uid = %d  AND hm.perms > %d ";
    $heartbeat->raw_messages = $this->resultSql($sql, array($this->stream->uid, HEARTBEAT_PRIVATE));
    return $heartbeat;
  }

  /**
   * Function to add a part of a sql to a query built by views UI
   *
   * @param object $view The view handler object by reference to add our part to the query
   */
  public function addViewQuery(&$view) {
    // Make the sql limited to the access
    $field = "$view->table_alias.$view->real_field";
    //$view->query->set_where_group('AND', 'andgroup');
    $sql = "$field = %d  AND $view->table_alias.access > %d ";
    $view->query->add_where('orgroup', $sql, $this->_uid, HEARTBEAT_PRIVATE);
  }
}